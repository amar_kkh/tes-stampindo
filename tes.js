var print = '';
var maxNumber = 100;
for (var i = 1; i <= maxNumber; i++) {
    var isMultipleOf3 = i % 3 == 0;
    var isMultipleOf5 = i % 5 == 0;
    if (isMultipleOf3 && isMultipleOf5) {
        print += 'ApaBole';
    } else if (isMultipleOf5) {
        print += 'Bole';
    } else if (isMultipleOf3) {
        print += 'Apa';
    } else {
        print += String(i);
    }
    print += ', ' //spacing
}
console.log(print)